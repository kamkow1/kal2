import sys
import os
import argparse
import uuid
from pathlib import Path
from subprocess import Popen, PIPE, STDOUT
from antlr4 import *
from generated.Kal2Lexer import Kal2Lexer
from generated.Kal2Parser import Kal2Parser
from generated.Kal2ParserVisitor import Kal2ParserVisitor

class Module:
    """
    represents a single translation unit
    """
    def __init__(self, name, std_path):
        self.name = name
        self.items = []
        self.types = {
            "int32": "int32_t",
            "uint32": "uint32_t",
            "uint8": "uint8_t",
            "char": "int8_t",
            "boolean": "int8_t",
            "void": "void",
        }
        # statements that will be called before calling the user-defined main function
        # this is used to make global variables work
        self.startup_statements = []
        self.prepend = ""
        self.is_main = False
        self.std_path = std_path

    def get_module_name(self, exclude_self=False):
        """transform a module's name into a c-preprocessor-compliant token
        this is needed for include guards and resolving module names (eg. module::some_func)
        """

        if exclude_self:
            module_name = Path(self.name).parent
        else:
            module_name = self.name
        file_name_no_ext = os.path.splitext(os.path.basename(module_name))[0]
        dir_name = Path(module_name).parent
        module_name = os.path.join(dir_name, file_name_no_ext)
        for ch in [".", "-", "\\", "/"]:
            module_name = module_name.replace(ch, "_")
        return module_name

    def make_main_func(self):
        """create an expanded template string representing the actual main function
        Here, main calls to a user defined main function
        """
        expanded_ss = "\n".join([item.__str__() for item in self.startup_statements])
        module_name = self.get_module_name()
        return f"""int32_t main(int32_t argc, char **argv)
{{
  {expanded_ss}
  {module_name + '__main(argc, (int8_t**)argv);'}
  return 0;
}}
"""

    def __str__(self):
        module_name = self.get_module_name()
        expanded_items = "\n".join([item.__str__() for item in self.items])
        main_func = self.make_main_func() if self.is_main else ""
        return f"""{self.prepend}

#ifndef {module_name}
#define {module_name}

#include <stdint.h>

{expanded_items}

{main_func}

#endif // {module_name}
"""

class FuncSignature:
    """represents a function signature
    NOTE: a function has to be declared separately from it's definition
    syntax:
        name :: func return_type (arg_name type_name);
    """
    
    def __init__(self, name, return_type, params, meta_directives):
        self.name = name
        self.return_type = return_type
        self.params = params
        self.meta_directives = meta_directives

    def external(self):
        return "external" in self.meta_directives

    def __str__(self):
        params = ", ".join([" ".join([param[1], param[0]]) for param in self.params])
        return f"{'extern ' if self.external() else ''}{self.return_type} {self.name}({params});"

class FuncDefinition:
    """represents a function definition
    syntax:
        name {
            ... // all function's parameters are still accessible here
        }
    """
    def __init__(self, signature, statements):
        self.signature = signature
        self.statements = statements

    def __str__(self):
        return f"""
{self.signature}
{{
{self.statements}
}}
"""

class FuncCall:
    """represents a function call
    syntax:
        name arg1 arg2 arg3; // if the function accepts parameters
        name .; // if the function is parameterless
    """
    def __init__(self, name, params, semicolon):
        self.name = name
        self.params = params
        self.semicolon = semicolon

    def __str__(self):
        params = ", ".join([param.__str__() for param in self.params])
        return f"{self.name}({params}){';' if self.semicolon else ''}"

class Assignment:
    """represents an assignment
    syntax:
        name := 4; // NOTE: type if automatically deduced, if possible.
                   // otherwise the compiler wil, inform the user
        name int32 := 66; // explicit type name
        name = 333; // reassignment
    """
    def __init__(self, name, true_type, value):
        self.name = name
        self.true_type = true_type
        self.value = value

    def __str__(self):
        if self.true_type is None:
            return f"{self.name} = {self.value};"
        else:
            return f"{self.true_type} {self.name} = {self.value};"

class GlobalVarSignature:
    """represents a global variable's signature
    NOTE: a global variable's signature is completely separate from it's definition
    syntax:
        name :: var type_name;
    """
    def __init__(self, name, type, meta_directives):
        self.name = name
        self.type = type
        self.meta_directives = meta_directives
    
    def external(self):
        return "external" in self.meta_directives

    def __str__(self):
        return f"{'extern' if self.external() else ''} {self.type} {self.name};"

class Return:
    """represents a return statement
    syntax:
        return expression;
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f"return {self.value};"

class If:
    def __init__(self, cond, statements):
        self.cond = cond
        self.statements = statements

    def __str__(self):
        return f"""if ({self.cond}) {{
{self.statements}
}}"""

class Elif:
    """represents an elif block
    syntax:
        elif {
            ...
        }
    """
    def __init__(self, cond, statements):
        self.cond = cond
        self.statements = statements

    def __str__(self):
        return f"""else if ({self.cond}) {{
{self.statements}
}}"""

class Else:
    """represents an else block
    syntax:
        else {
            ...
        }
    """
    def __init__(self, statements):
        self.statements = statements

    def __str__(self):
        return f"""else {{
{self.statements}
}}"""

class For:
    """represents an for loop block
    syntax:
        for statement1; statement2; statement3; {
            ...
        }
    """
    def __init__(self, first, second, third, statements):
        self.statements = statements
        self.first = first
        self.second = second
        self.third = third

    def __str__(self):
        if self.first[-1] != ";":
            self.first += ";"

        if self.second[-1] != ";":
            self.second += ";"

        if self.third[-1] == ";":
            self.third = self.third[:-1]

        return f"""for ({self.first}
{self.second}
{self.third}) {{
{self.statements}
}}"""

class While:
    """represents an while loop block
    syntax:
        while condition {
            ...
        }
    """
    def __init__(self, condition, statements):
        self.statements = statements
        self.condition = condition

    def __str__(self):
        return f"""while (({self.condition})) {{
{self.statements}
}}"""

class Comparison:
    """represents a comparison expression
    syntax:
        <operator> <operand1> <operand2>
        == 1 2
        >  1 2
        <  1 2
        >= 1 2
        <= 1 2
    """
    def __init__(self, lhs, rhs, cmp):
        self.lhs = lhs
        self.rhs = rhs
        self.cmp = cmp

    def __str__(self):
        return f"({self.lhs} {self.cmp} {self.rhs})"

class Negation:
    """represents a negation expression
    syntax:
        ! expression
        ! == 1 2 // same as != in other programming languages
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f"!({self.value})"

class Logic:
    """represents a logical operation
    syntax:
        <operator> <operand1> <operand2>
        && a b
        || a b
    """
    def __init__(self, lhs, rhs, op):
        self.lhs = lhs
        self.rhs = rhs
        self.op = op

    def __str__(self):
        return f"({self.lhs} {self.op} {self.rhs})"

class BitLogic:
    """represents a bitwise logical operation
    syntax:
        <operator> <operand1> <operand2>
        & a b
        | a b
    """
    def __init__(self, lhs, rhs, op):
        self.lhs = lhs
        self.rhs = rhs
        self.op = op

    def __str__(self):
        return f"({self.lhs} {self.op} {self.rhs})"

class Arithmetic:
    """represents an arithemtic operation
    syntax:
        <operator> <operand1> <operand2>
        + a b
        - a b
        / a b
        * a b
        % a b
    """
    def __init__(self, lhs, rhs, op):
        self.lhs = lhs
        self.rhs = rhs
        self.op = op

    def __str__(self):
        return f"({self.lhs} {self.op} {self.rhs})"

class CInclude:
    """represents a #c_include directive
    This expands into C's #include "header-name.h"
    See tests/ directory for more info on directives
    """
    def __init__(self, inc_path):
        self.inc_path = inc_path

    def __str__(self):
        return f"#include {self.inc_path}"

class Cast:
    """represents a type casting expression
    syntax:
        cast type_name expression
    """
    def __init__(self, type, value):
        self.type = type
        self.value = value

    def __str__(self):
        return f"(({self.type}){self.value})"

class Array:
    """represents an array expression
    syntax:
        [expr1 expr2 expr2 [expr3 expr4]] // no commans needed
    """
    def __init__(self, ref):
        self.ref = ref

    def __str__(self):
        return f"({self.ref})"

class ArrayAccess:
    """represents an array access expression
    syntax:
        [] array index
    """
    def __init__(self, array, index):
        self.array = array
        self.index = index

    def __str__(self):
        return f"{self.array}[{self.index}]"

class StructSignature:
    """represents a structure signature/declaration
    NOTE: this is separate from a structure definition
    syntax:
        Struct_Name :: struct;
    """
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"typedef struct {self.name} {self.name};"

class StructDefinition:
    """represents a structure definition
    syntax:
        Struct_Name {
            (field1 type)
            (field2 type)
        }
    """
    def __init__(self, name, fields):
        self.name = name
        self.fields = fields

    def __str__(self):
        return f"""struct {self.name} {{
{self.fields}
}};
"""

class StructFieldInitializer:
    """represents a field initialization inside of a struct initializer
    syntax:
        (field expr)
    """
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __str__(self):
        return f".{self.name} = {self.value}"

class StructInitializer:
    """represents an initialization of a struct
    syntax:
        Struct_Name {
            (field expr)
            (field2 expr)
        }
    """
    def __init__(self, name, fields):
        self.name = name
        self.fields = fields

    def __str__(self):
        return f"""({self.name}) {{ {self.fields} }}
"""

class StructFieldAccess:
    """represents a struct field access expression
    syntax:
        -> expr field_name
        *-> expr field_name // expr must be a pointer to a struct
    """
    def __init__(self, name, value, deref):
        self.name = name
        self.value = value
        self.deref = deref

    def __str__(self):
        op = "->" if self.deref else "."
        return f"({self.value}{op}{self.name})"

class Address:
    """represents an 'address of' expression
    syntax:
        &expression
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f"(&{self.value})"

class Visitor(Kal2ParserVisitor):
    def __init__(self, module_name, std_path):
        self.module = Module(module_name, std_path)
        self.current_func = None

    def visitFile(self, ctx: Kal2Parser.FileContext):
        for statement in ctx.statement():
            self.visit(statement)
    
    def visitStatement(self, ctx: Kal2Parser.StatementContext):
        return self.visit(ctx.getChild(0))
    
    def visitMetaDirective(self, ctx: Kal2Parser.MetaDirectiveContext):
        return ctx.Ident().getText()

    def visitFuncSignature(self, ctx: Kal2Parser.FuncSignatureContext):
        params = [self.visit(param) for param in ctx.funcSignatureParam()]
        return_type = self.visit(ctx.typeName())
        meta_directives = [self.visit(d) for d in ctx.metaDirective()]
        if "external" in meta_directives:
            name = ctx.Ident().getText()
        else:
            name = self.module.get_module_name() + "__" + ctx.Ident().getText()
        self.module.items.append(FuncSignature(name, return_type, params, meta_directives))

    def visitFuncSignatureParam(self, ctx: Kal2Parser.FuncSignatureParamContext):
        if ctx.tripleDot():
            return ("...", "")
        else:
            name = self.module.get_module_name() + "__" + ctx.Ident().getText()
            type = self.visit(ctx.typeName())
            return (name, type)

    def visitFuncDefinition(self, ctx: Kal2Parser.FuncDefinitionContext):
        name = self.module.get_module_name() + "__" + ctx.Ident().getText()
        self.current_func = name
        statements = ""
        for statement in ctx.statement():
            string = self.visit(statement).__str__()
            lines = ["  " + line for line in string.split("\n")]
            statements += "\n".join(lines) + "\n"
        self.current_func = None

        signature = None
        for item in self.module.items:
            if type(item).__name__ == "FuncSignature":
                if item.name == name:
                    signature = item.__str__()[:-1]
        if not signature:
            print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: tried to define an undeclared function")
            exit(1)
        dfn = FuncDefinition(signature, statements)
        self.module.items.append(dfn)
    
    def visitFuncCall(self, ctx: Kal2Parser.FuncCallContext):
        is_std = False
        for md in ctx.metaDirective():
            directive_name = self.visit(md)
            if directive_name == "std":
                is_std = True

        base_name = ctx.Ident()[-1].getText()

        if len(ctx.Ident()) > 1:
            # the function is imported form another module
            if is_std:
                std_path = self.module.std_path
                for ch in [".", "-", "\\", "/"]:
                    std_path = std_path.replace(ch, "_")
                components = "_".join([component.getText() for component in ctx.Ident()[1:-1]])
                name = std_path + "_" + components + "__" + base_name
            else:
                components = "_".join([component.getText() for component in ctx.Ident()[:-1]])
                module_name = self.module.get_module_name(exclude_self=True)
                name = module_name + "_" + components + "__" + base_name
        else:
            found_locally = False
            for item in self.module.items:
                if type(item).__name__ == "FuncSignature":
                    full_name = self.module.get_module_name() + "__" + base_name
                    if item.name == base_name and item.external():
                        found_locally = True
                        name = base_name
                    elif item.name == full_name:
                        found_locally = True
                        name = full_name
            if not found_locally:
                name = base_name
        params = [self.visit(expr) for expr in ctx.expression()]
        semicolon = False
        if ctx.parentCtx.parentCtx:
            if type(ctx.parentCtx.parentCtx).__name__ == "StatementContext":
                # single expression as a statement will need a semicolon
                # example:
                # some_func(); <-- `some_func()` is an expression, but is used like a statement
                semicolon = True
        return FuncCall(name, params, semicolon)
    
    def visitExpression(self, ctx: Kal2Parser.ExpressionContext):
        if ctx.Integer():
            return int(ctx.Integer().getText())
        if ctx.Ident():
            is_std = False
            for md in ctx.metaDirective():
                directive_name = self.visit(md)
                if directive_name == "std":
                    is_std = True

            base_name = ctx.Ident()[-1].getText()

            if len(ctx.Ident()) > 1:
                # the function is imported form another module

                if is_std:
                    std_path = self.module.std_path
                    for ch in [".", "-", "\\", "/"]:
                        std_path = std_path.replace(ch, "_")
                    components = "_".join([component.getText() for component in ctx.Ident()[1:-1]])
                    name = std_path + "_" + components + "__" + base_name
                else:
                    components = "_".join([component.getText() for component in ctx.Ident()[:-1]])
                    module_name = self.module.get_module_name(exclude_self=True)
                    name = module_name + "_" + components + "__" + base_name
            else:
                # name = self.module.get_module_name() + "__" + base_name
                found_locally = False
                for item in self.module.items:
                    if type(item).__name__ == "GlobalVarSignature":
                        full_name = self.module.get_module_name() + "__" + base_name
                        if item.name == base_name and item.external():
                            found_locally = True
                            name = base_name
                        elif item.name == full_name:
                            found_locally = True
                            name = full_name
                if not found_locally:
                    name = self.module.get_module_name() + "__" + base_name
            return name
        if ctx.funcCall():
            return self.visit(ctx.funcCall())
        if ctx.comparison():
            return self.visit(ctx.comparison())
        if ctx.negation():
            return self.visit(ctx.negation())
        if ctx.logic():
            return self.visit(ctx.logic())
        if ctx.bitLogic():
            return self.visit(ctx.bitLogic())
        if ctx.emphExpression():
            return self.visit(ctx.emphExpression())
        if ctx.String():
            return ctx.String().getText()
        if ctx.arithmetic():
            return self.visit(ctx.arithmetic())
        if ctx.casting():
            return self.visit(ctx.casting())
        if ctx.array():
            return self.visit(ctx.array())
        if ctx.arrayAccess():
            return self.visit(ctx.arrayAccess())
        if ctx.structInitializer():
            return self.visit(ctx.structInitializer())
        if ctx.structFieldAccess():
            return self.visit(ctx.structFieldAccess())
        if ctx.address():
            return self.visit(ctx.address())
    
    def visitEmphExpression(self, ctx: Kal2Parser.EmphExpressionContext):
        return self.visit(ctx.expression())

    def visitArray(self, ctx: Kal2Parser.ArrayContext):
        exprs = []
        for expr in ctx.expression():
            value = self.visit(expr)
            if type(value).__name__ == "Array":
                exprs.append(value.ref)
            else:
                exprs.append(value.__str__())
        exprs = ", ".join(exprs)
        array_type = self.visit(ctx.typeName())
        id = str(uuid.uuid4()).split('-')[0]
        name = f"_array_{id}"
        self.module.items.append(f"{array_type} {name}[] = {{ {exprs} }};")
        return Array(name)
    
    def visitArrayAccess(self, ctx: Kal2Parser.ArrayAccessContext):
        array = self.visit(ctx.expression()[0])
        index = self.visit(ctx.expression()[1])
        return ArrayAccess(array, index)
    
    def visitAssignment(self, ctx: Kal2Parser.AssignmentContext):
        name = self.module.get_module_name() + "__" + ctx.Ident().getText()
        expr = self.visit(ctx.expression())

        if not self.current_func:
            self.module.startup_statements.append(Assignment(name, None, expr))

        if ctx.Colon():
            if not ctx.typeName():
                if type(expr).__name__ == "int":
                    true_type = self.module.types[self.module.get_module_name() + "__int32"]
                elif type(expr).__name__ == "FuncCall":
                    found = False
                    for item in self.module.items:
                        if type(item).__name__ == "FuncSignature":
                            if item.name == expr.name:
                                true_type = item.return_type
                                found = True
                    if not found:
                        print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Cannot implicity deduce function return type")
                        exit(1)
                elif type(expr).__name__ == "Comparison":
                    true_type = self.module.types[self.module.get_module_name() + "__boolean"]
                elif type(expr).__name__ == "Negation":
                    true_type = self.module.types[self.module.get_module_name() + "__boolean"]
                elif type(expr).__name__ == "Logic":
                    true_type = self.module.types[self.module.get_module_name() + "__boolean"]
                elif type(expr).__name__ == "BitLogic":
                    print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: bitwise logic operators require a type")
                    exit(1)
                else:
                    print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Cannot implicity deduce the expression type")
                    exit(1)
            else:
                true_type = self.visit(ctx.typeName())
            return Assignment(name, true_type, expr)
        else:
            return Assignment(name, None, expr)
    
    def visitGlobalVarSignature(self, ctx: Kal2Parser.GlobalVarSignatureContext):
        name = self.module.get_module_name() + "__" + ctx.Ident().getText()
        type = self.visit(ctx.typeName())
        meta_directives = [self.visit(md) for md in ctx.metaDirective()]
        self.module.items.append(GlobalVarSignature(name, type, meta_directives))
    
    def visitReturnStatement(self, ctx:Kal2Parser.ReturnStatementContext):
        return Return(self.visit(ctx.expression()))
    
    def visitIfStatement(self, ctx: Kal2Parser.IfStatementContext):
        cond = self.visit(ctx.expression())
        statements = ""
        for statement in ctx.statement():
            string = self.visit(statement).__str__()
            lines = ["  " + line for line in string.split("\n")]
            statements += "\n".join(lines) + "\n"

        return If(cond, statements)
    
    def visitElifStatement(self, ctx:Kal2Parser.ElifStatementContext):
        cond = self.visit(ctx.expression())
        statements = ""
        for statement in ctx.statement():
            string = self.visit(statement).__str__()
            lines = ["  " + line for line in string.split("\n")]
            statements = "\n".join(lines)
        return Elif(cond, statements)
    
    def visitElseStatement(self, ctx: Kal2Parser.ElseStatementContext):
        statements = ""
        for statement in ctx.statement():
            string = self.visit(statement).__str__()
            lines = ["  " + line for line in string.split("\n")]
            statements = "\n".join(lines)
        return Else(statements)
    
    def visitForStatement(self, ctx: Kal2Parser.ForStatementContext):
        first = self.visit(ctx.statement()[0]).__str__()
        second = self.visit(ctx.statement()[1]).__str__()
        third = self.visit(ctx.statement()[2]).__str__()
        rest = ctx.statement()[3:]
        statements = ""
        for statement in rest:
            string = self.visit(statement).__str__()
            lines = ["  " + line for line in string.split("\n")]
            statements += "\n".join(lines) + "\n"
        return For(first, second, third, statements)
    
    def visitWhileStatement(self, ctx: Kal2Parser.WhileStatementContext):
        condition = self.visit(ctx.expression()).__str__()
        statements = ""
        for statement in ctx.statement():
            string = self.visit(statement).__str__()
            lines = ["  " + line for line in string.split("\n")]
            statements += "\n".join(lines) + "\n"
        return While(condition, statements)
    
    def visitComparison(self, ctx: Kal2Parser.ComparisonContext):
        lhs = self.visit(ctx.expression()[0])
        rhs = self.visit(ctx.expression()[1])
        return Comparison(lhs, rhs, ctx.compOperator().getText())
    
    def visitNegation(self, ctx: Kal2Parser.NegationContext):
        return Negation(self.visit(ctx.expression()))
    
    def visitLogic(self, ctx:Kal2Parser.LogicContext):
        lhs = self.visit(ctx.expression()[0])
        rhs = self.visit(ctx.expression()[1])
        return Logic(lhs, rhs, ctx.logicOperator().getText())
    
    def visitBitLogic(self, ctx:Kal2Parser.BitLogicContext):
        lhs = self.visit(ctx.expression()[0])
        rhs = self.visit(ctx.expression()[1])
        return BitLogic(lhs, rhs, ctx.bitLogicOperator().getText())

    def visitArithmetic(self, ctx:Kal2Parser.ArithmeticContext):
        lhs = self.visit(ctx.expression()[0])
        rhs = self.visit(ctx.expression()[1])
        return Arithmetic(lhs, rhs, ctx.arithmeticOperator().getText())
   
    def visitArrayType(self, ctx: Kal2Parser.ArrayTypeContext):
        return f"[{(ctx.Integer().getText())}]"

    def visitComplexType(self, ctx: Kal2Parser.ComplexTypeContext):
        if ctx.Asterisk():
            return "*"
        if ctx.arrayType():
            return self.visit(ctx.arrayType())

    def visitTypeName(self, ctx: Kal2Parser.TypeNameContext):
        is_std = False
        for md in ctx.metaDirective():
            directive_name = self.visit(md)
            if directive_name == "std":
                is_std = True
        
        base_name = ctx.Ident()[-1].getText()
        
        if len(ctx.Ident()) > 1:
            # the struct is imported form another module
            if is_std:
                std_path = self.module.std_path
                for ch in [".", "-", "\\", "/"]:
                    std_path = std_path.replace(ch, "_")
                components = "_".join([component.getText() for component in ctx.Ident()[1:-1]])
                name = std_path + "_" + components + "__" + base_name
            else:
                components = "_".join([component.getText() for component in ctx.Ident()[:-1]])
                module_name = self.module.get_module_name(exclude_self=True)
                name = module_name + "_" + components + "__" + base_name
        else:
            found_locally = False
            for item in self.module.items:
                if type(item).__name__ == "StructSignature":
                    full_name = self.module.get_module_name() + "__" + base_name
                    if item.name == base_name:
                        found_locally = True
                        name = base_name
                    elif item.name == full_name:
                        found_locally = True
                        name = full_name
            if not found_locally:
                name = base_name
        if name in self.module.types:
            name = self.module.types[name]

        # base_name = self.module.get_module_name() + "__" + ctx.Ident().getText()
        # type = self.module.types[base_name]
        for complexType in ctx.complexType():
            c = self.visit(complexType)
            if c == "*":
                name += c
            else:
                name += " " + c
        return name
    
    def visitCasting(self, ctx: Kal2Parser.CastingContext):
        type = self.visit(ctx.typeName())
        value = self.visit(ctx.expression())
        return Cast(type, value)
    
    def visitAddress(self, ctx:Kal2Parser.AddressContext):
        return Address(self.visit(ctx.expression()).__str__())
    
    def visitMetaDirectiveStatement(self, ctx: Kal2Parser.MetaDirectiveStatementContext):
        directive_name = ctx.metaDirective().Ident().getText();

        if directive_name == "c_include":
            if len(ctx.expression()) > 1:
                print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Too many arguments for `c_include`")
                exit(1)
            elif len(ctx.expression()) < 1:
                print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Not enough arguments for `c_include`. Expected 1")
                exit(1)
            else:
                current_dir = os.path.abspath(os.path.dirname(os.path.abspath(self.module.name)))
                module = self.visit(ctx.expression()[0])[1:-1]
                split_module = module.split(os.sep)
                if split_module[0] == "#std":
                    rest = split_module[1:]
                    inc_path = os.path.join(self.module.std_path, *rest)
                else:
                    inc_path = os.path.join(current_dir, module)
                inc_path = '"' + inc_path + '"'
                # inc_path = self.visit(ctx.expression()[0])
                self.module.items.append(CInclude(inc_path))
        elif directive_name == "c_include_system":
            if len(ctx.expression()) > 1:
                print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Too many arguments for `c_system`")
                exit(1)
            elif len(ctx.expression()) < 1:
                print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Not enough arguments for `c_system`. Expected 1")
                exit(1)
            else:
                inc_path = self.visit(ctx.expression()[0])
                self.module.items.append(CInclude(inc_path))
        elif directive_name == "import":
            if len(ctx.expression()) > 1:
                print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Too many arguments for `import`")
                exit(1)
            elif len(ctx.expression()) < 1:
                print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Not enough arguments for `import`. Expected 1")
                exit(1)
            else:
                current_dir = os.path.abspath(os.path.dirname(os.path.abspath(self.module.name)))
                module = self.visit(ctx.expression()[0])[1:-1]
                split_module = module.split(os.sep)
                if split_module[0] == "#std":
                    rest = split_module[1:]
                    import_path = os.path.join(self.module.std_path, *rest)
                else:
                    import_path = os.path.join(current_dir, module)
                with open(import_path) as f:
                    content = f.read()
                    lexer = Kal2Lexer(InputStream(content))
                    tokens = CommonTokenStream(lexer)
                    parser = Kal2Parser(tokens)
                    tree = parser.file_()
                    temp_visitor = Visitor(import_path, self.module.std_path)
                    temp_visitor.visit(tree)
                    self.module.prepend += temp_visitor.module.__str__()
        elif directive_name == "main_module":
            if len(ctx.expression()) != 0:
                print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Too many arguments for `main`")
                exit(1)
            else:
                self.module.is_main = True
        elif directive_name == "import_dir":
            if len(ctx.expression()) > 1:
                print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Too many arguments for `import_dir`")
                exit(1)
            elif len(ctx.expression()) < 1:
                print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Not enough arguments for `import_dir`. Expected 1")
                exit(1)
            else:
                current_dir = os.path.abspath(os.path.dirname(os.path.abspath(self.module.name)))
                dir = os.path.join(current_dir, self.visit(ctx.expression()[0])[1:-1])
                for subdir, dirs, files in os.walk(dir):
                    for file in files:
                        import_path = os.path.join(dir, file)
                        with open(import_path) as f:
                            content = f.read()
                            lexer = Kal2Lexer(InputStream(content))
                            tokens = CommonTokenStream(lexer)
                            parser = Kal2Parser(tokens)
                            tree = parser.file_()
                            temp_visitor = Visitor(import_path)
                            temp_visitor.visit(tree)
                            self.module.prepend += temp_visitor.module.__str__()
        else:
            print(f"{sys.argv[1]}:{ctx.start.line}:{ctx.start.column}: Unknown directive `{directive_name}`")
            exit(1)
    
    def visitStructSignature(self, ctx: Kal2Parser.StructSignatureContext):
        name = self.module.get_module_name() + "__" + ctx.Ident().getText()
        self.module.types[name] = name
        self.module.items.append(StructSignature(name))
    
    def visitStructField(self, ctx: Kal2Parser.StructFieldContext):
        name = ctx.Ident().getText()
        type = self.visit(ctx.typeName())
        return (name, type)

    def visitStructDefinition(self, ctx: Kal2Parser.StructDefinitionContext):
        name = self.module.get_module_name() + "__" + ctx.Ident().getText()
        fields = [self.visit(field) for field in ctx.structField()]
        fields = "\n".join([field[1] + " " + field[0] + ";" for field in fields])
        self.module.items.append(StructDefinition(name, fields))
    
    def visitStructInitializer(self, ctx: Kal2Parser.StructInitializerContext):
        is_std = False
        for md in ctx.metaDirective():
            directive_name = self.visit(md)
            if directive_name == "std":
                is_std = True
        
        base_name = ctx.Ident()[-1].getText()
        
        if len(ctx.Ident()) > 1:
            # the struct is imported form another module
            if is_std:
                std_path = self.module.std_path
                for ch in [".", "-", "\\", "/"]:
                    std_path = std_path.replace(ch, "_")
                components = "_".join([component.getText() for component in ctx.Ident()[1:-1]])
                name = std_path + "_" + components + "__" + base_name
            else:
                components = "_".join([component.getText() for component in ctx.Ident()[:-1]])
                module_name = self.module.get_module_name(exclude_self=True)
                name = module_name + "_" + components + "__" + base_name
        else:
            found_locally = False
            for item in self.module.items:
                if type(item).__name__ == "StructSignature":
                    full_name = self.module.get_module_name() + "__" + base_name
                    if item.name == base_name:
                        found_locally = True
                        name = base_name
                    elif item.name == full_name:
                        found_locally = True
                        name = full_name
            if not found_locally:
                name = base_name

        fields = ",\n".join([self.visit(field).__str__() for field in ctx.structFieldInitializer()])
        return StructInitializer(name, fields)

    def visitStructFieldInitializer(self, ctx: Kal2Parser.StructFieldInitializerContext):
        name = ctx.Ident().getText()
        value = self.visit(ctx.expression())
        if type(value).__name__ == "Array":
            value = value.ref
        else:
            value = value.__str__()
        return StructFieldInitializer(name, value)
    
    def visitStructFieldAccess(self, ctx: Kal2Parser.StructFieldAccessContext):
        name = ctx.Ident().getText()
        value = self.visit(ctx.expression()).__str__()
        deref = ctx.DerefArrow() != None
        return StructFieldAccess(name, value, deref)

# CONFIG
cc = "gcc"

def main():
    global cc

    parser = argparse.ArgumentParser()
    parser.add_argument("source_file", type=str, help="input file *.kal2")
    parser.add_argument("-dump_c", action="store_true", help="write generated C code to a file")
    parser.add_argument("-cc_flags", type=str, help="custom CC flags")
    parser.add_argument("-cc", type=str, help="custom CC executable. default is gcc")
    parser.add_argument("-std", type=str, help="path to the standard library directory")
    args = parser.parse_args()

    input = FileStream(args.source_file)

    lexer = Kal2Lexer(input)
    tokens = CommonTokenStream(lexer)
    parser = Kal2Parser(tokens)
    tree = parser.file_()
    visitor = Visitor(os.path.abspath(sys.argv[1]), std_path=args.std)
    visitor.visit(tree)

    filename_no_ext = os.path.splitext(os.path.basename(sys.argv[1]))[0]
    if args.dump_c:
        with open(filename_no_ext + ".c", "w") as f:
            f.write(visitor.module.__str__())

    cc_flags = [
        "-v",
        "-o", filename_no_ext,
        "-std=c99",
        "-Wall", "-Wextra",
        "-xc", "-"
    ]
    if args.cc_flags:
        cc_flags = args.cc_flags.split(" ")
        cc_flags.append("-xc")
        cc_flags.append("-")

    if args.cc:
        cc = args.cc

    p = Popen([cc, *cc_flags], stdout=sys.stdout, stdin=PIPE, stderr=sys.stdout)
    p.communicate(input=visitor.module.__str__().encode())

if __name__ == '__main__':
    main()

