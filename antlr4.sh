#!/bin/sh

antlr4_jar=$HOME/.m2/repository/org/antlr/antlr4/4.13.2-SNAPSHOT/antlr4-4.13.2-SNAPSHOT-complete.jar

java -jar "$antlr4_jar" -Dlanguage=Python3 -no-listener -visitor -o generated Kal2Lexer.g4
java -jar "$antlr4_jar" -Dlanguage=Python3 -no-listener -visitor -o generated Kal2Parser.g4

