parser grammar Kal2Parser;

options {
    tokenVocab = 'generated/Kal2Lexer';
}

file: statement*;

metaDirective: Hash Ident;

statement: funcSignature Semicolon
    |   funcDefinition
    |   structSignature Semicolon
    |   structDefinition
    |   globalVarSignature Semicolon
    |   assignment Semicolon
    |   returnStatement Semicolon
    |   expression Semicolon
    |   ifStatement
    |   elifStatement
    |   elseStatement
    |   forStatement
    |   whileStatement
    |   metaDirectiveStatement Semicolon
    ;

expression: Integer
    |   metaDirective* (Ident Colon Colon)* Ident
    |   String
    |   structInitializer
    |   structFieldAccess
    |   arrayAccess
    |   array
    |   casting
    |   address
    |   funcCall
    |   comparison
    |   negation
    |   logic
    |   bitLogic
    |   arithmetic
    |   emphExpression
    ;

emphExpression: OpeningParen expression ClosingParen;

casting: Cast typeName expression;

address: And expression;

structSignature: Ident Colon Colon Struct;
structDefinition: Ident OpeningBrace structField* ClosingBrace;
structField: OpeningParen Ident typeName ClosingParen;
structInitializer: metaDirective* (Ident Colon Colon)* Ident OpeningBrace structFieldInitializer* ClosingBrace;
structFieldInitializer: OpeningParen Ident expression ClosingParen;
structFieldAccess: (Arrow | DerefArrow) expression Ident;

array: typeName OpeningSqbr expression* ClosingSqbr;
arrayAccess: OpeningSqbr ClosingSqbr expression expression;

funcSignature: Ident Colon Colon metaDirective* Func typeName funcSignatureParam*;
tripleDot: Dot Dot Dot;
funcSignatureParam: OpeningParen ((Ident typeName) | tripleDot) ClosingParen;
funcDefinition: Ident OpeningBrace statement* ClosingBrace;
funcCall: metaDirective* (Ident Colon Colon)* Ident (expression* | Dot);

assignment: Ident typeName? Colon? EqualitySign expression;

globalVarSignature: metaDirective* Ident Colon Colon Var typeName;

returnStatement: Return expression;
ifStatement: If expression OpeningBrace statement* ClosingBrace;
elifStatement: Elif expression OpeningBrace statement* ClosingBrace;
elseStatement: Else OpeningBrace statement* ClosingBrace;
forStatement: For statement statement statement OpeningBrace statement* ClosingBrace;
whileStatement: While expression OpeningBrace statement* ClosingBrace;

compOperator: Equ | Lt | Gt | Loq | Goq;
comparison: compOperator expression expression;
negation: ExclMark expression;
logicOperator: Lor | Land;
logic: logicOperator expression expression;
bitLogicOperator: Pipe | And;
bitLogic: bitLogicOperator expression expression;
arithmeticOperator: Plus | Minus | Slash | Asterisk | Percentage;
arithmetic: arithmeticOperator expression expression;

arrayType: OpeningSqbr Integer ClosingSqbr;
complexType: Asterisk | arrayType;
typeName: metaDirective* complexType* (Ident Colon Colon)* Ident;

metaDirectiveStatement: metaDirective expression*;

