#ifndef STD_IO_MACROS_H_
#define STD_IO_MACROS_H_

#include <stdio.h>

#define print(fmt, ...) \
{ \
    fprintf(stdout, fmt, ##__VA_ARGS__); \
} \

#define eprint(fmt, ...) \
{ \
    fprintf(stderr, fmt, ##__VA_ARGS__); \
} \

#endif // STD_IO_MACROS_H_
